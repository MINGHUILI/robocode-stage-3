package myrobot;

import robocode.*;
import robocode.Robot;
import java.awt.*;

public class Hunter1 extends AdvancedRobot {
    boolean movingForward;
    int moveDirection = 1;
	public static double PI = Math.PI;
	Enemy enemy = new Enemy();
    public void run() {
	    
        setAdjustGunForRobotTurn(true);
        setBodyColor(Color.blue);
        setGunColor(Color.green);
        setRadarColor(Color.pink);
        setScanColor(Color.green);
        setBulletColor(Color.white);
        setAdjustRadarForGunTurn(true);

        while (true) {
            movingForward = true;
            turnRadarRightRadians(5);
            if (enemy.name == null) {
                setTurnRadarRightRadians(2 * PI);
                execute();
            } else {
                execute();

            }
        }

    }
    public void onHitWall(HitWallEvent e) {

        //moveDirection = -moveDirection;
        reverseDirection();
    }

    public void reverseDirection() {
        if (movingForward) {
            setBack(10000);
            movingForward = false;
        } else {
            setAhead(10000);
            movingForward = true;
        }
    }

    public void onHitRobot(HitRobotEvent e) {
       /* if (e.isMyFault()) {
            reverseDirection();
			setBack(100);
            fire(2);
        }*/
	   moveDirection=-moveDirection;
	   fire(2);
       /* setBack(200);
        setFire(2);
        turnRight(45);*/
    }

    public void onBulletHit(BulletHitEvent event) {
        double energy = event.getEnergy();
        if (energy < event.getEnergy()) {
            moveDirection = -moveDirection;
            setFire(2);
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {

        enemy.update(e, this);
        double Offset = rectify(enemy.direction - getRadarHeadingRadians());
        double absBearing = 0d;

        double radarTurnAngle = 0d;

        double gunTurnAngle = 0d;

        double bodyTurnAngle = 0d;

        absBearing = robocode.util.Utils.normalRelativeAngle(e.getBearingRadians() + getHeadingRadians());

        radarTurnAngle = Math.sin(absBearing - getRadarHeadingRadians());

        gunTurnAngle = Math.sin(absBearing - getGunHeadingRadians());

        bodyTurnAngle = Math.sin(absBearing - getHeadingRadians());
        setTurnRadarRightRadians(radarTurnAngle);

        setTurnGunRightRadians(gunTurnAngle);

        setTurnRightRadians(bodyTurnAngle);


        setAhead(e.getDistance());

        if (getGunHeat() < 0.1) setFire(2);
    }
	
public double rectify(double angle) {
        if (angle < -Math.PI)

            angle += 2 * Math.PI;

        if (angle > Math.PI)

            angle -= 2 * Math.PI;

        return angle;

    }
	
public class Enemy {
        public double x, y;

        public String name = null;

        public double headingRadian = 0.0D;

        public double bearingRadian = 0.0D;

        public double distance = 1000D;

        public double direction = 0.0D;

        public double velocity = 0.0D;

        public double prevHeadingRadian = 0.0D;

        public double energy = 100.0D;


        public void update(ScannedRobotEvent e, AdvancedRobot Hunter) {
            name = e.getName();

            headingRadian = e.getHeadingRadians();

            bearingRadian = e.getBearingRadians();

            this.energy = e.getEnergy();

            this.velocity = e.getVelocity();

            this.distance = e.getDistance();

            direction = bearingRadian + Hunter.getHeadingRadians();

            x = Hunter.getX() + Math.sin(direction) * distance;

            y = Hunter.getY() + Math.cos(direction) * distance;

        }

    }
}


